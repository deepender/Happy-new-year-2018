import Vue from 'vue'
import VueRouter from 'vue-router' 

import App from './App.vue'
import NewYear from './components/NewYear.vue'

Vue.use(VueRouter) 

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '*',
      component: NewYear
     },
     { path: '/:id/:fromName', component: NewYear }
    ]
});

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})
